<?php

/**
 * @file
 * senangpay menu items.
 */

/**
 * Finalizes senangpay transaction.
 */

function uc_senangpay_complete($cart_id = 0) {
  watchdog('Senangpay', 'Receiving order payment notification for order !order_id.', array('!order_id' => check_plain($_REQUEST['order_id'])));

  $localsecret = variable_get('uc_senangpay_secret_word', '');
  $oid_prefix =  variable_get('uc_senangpay_oid_prefix', '');

  if (!empty($oid_prefix)) {
    $oid = explode('-',$_REQUEST['order_id']);
    $order_id = $oid[1];
    
  } else {
    $order_id = $_REQUEST['order_id'];
  }

  $order = uc_order_load($order_id);
	//dpm($order);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    return t('An error has occurred during payment.  Please contact us to ensure your order has been submitted.');
  }

  $status_id = $_REQUEST['status_id']; 
  $transID = $_REQUEST['transaction_id'];
  $amount = $_REQUEST['amt'];
  $hash = $_REQUEST['hash'];


  //$localhash = md5($localsecret.$status_id.$order_id.$transID.'Payment_was_successful');
  // assume the format is ?order_id=[ORDER_ID]&status_id=[TXN_STATUS]&transaction_id=[TXN_REF]&amt=[AMOUNT]&hash=[HASH]
  $localhash = md5($localsecret.'?order_id='.$order_id.'&status_id='.$status_id.'&transaction_id='.$transID.'&amt='.$amount.'&hash=[HASH]');

  // Save changes to order without it's completion.
  uc_order_save($order);

    if ($status_id == '1' && ($localhash  ==  $hash)) {
	//if ($status_id == '1' ) {
    $comment = t('Paid by !type, transaction reference #!txnref.', array('!type' => t('SenangPay'), '!txnref' => check_plain($transID)));
    uc_payment_enter($order_id, 'uc_senangpay',$amount, 0, NULL, $comment);
  	
	/*
	}
  	else {
    drupal_set_message(t('Your order will be processed as soon as your payment clears at senangpay.my.'));
    uc_order_comment_save($order->orderid, 0, t('!type payment is pending approval at senangpay.my.', array('!type' => $_REQUEST['channel'] == 'CC' ? t('Credit card') : t('eCheck'))), 'admin');
  }
  */
  
    // Empty that cart...
  uc_cart_empty($cart_id);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order_id, 0, t('Order created through website.'), 'admin');

  $build = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  $page = variable_get('uc_cart_checkout_complete_page', '');

  if (!empty($page)) {
    drupal_goto($page);
  }

	}
	// payment failed, redirect to cart
	else {
    drupal_set_message(t('Your payment was not successful. Please retry your order'),'error');
    uc_order_comment_save($order_id, 0, t('!type payment failed or pending approval at SenangPay', array('!type' => t('SenangPay'))), 'admin');
	drupal_goto('cart');
  }


  return $build;
}


?>